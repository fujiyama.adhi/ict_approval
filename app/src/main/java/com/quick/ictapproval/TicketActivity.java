package com.quick.ictapproval;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.quick.ictapproval.Config.Config;
import com.quick.ictapproval.Config.ConnectMySql;
import com.quick.ictapproval.Config.SharedPrefManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TicketActivity extends AppCompatActivity {
    TextView tv_issue_summary, tv_issue_Purpose, tv_issue_Detail,
            tv_no_order, tv_tanggal, tv_nama, tv_email, tv_voip, tv_seksi, tv_unit, tv_departemen, tv_sudah, tv_total;
    Button bt_approve, bt_tidak_setuju, bt_ubahApp;
    CheckBox cb_setuju;
    CardView cv_attach;
    ProgressBar pb_setuju;
    LinearLayout ll_tidak, ll_attach;
    SharedPrefManager SP_Help;
    Connection mConn;
    String line = null, result_upload;
    String img, status, issue_detail, issue_purpose, issue_summary;
    String no_order, tanggal, nama, email, voip,
            seksi, unit, departemen, ticket_id, id_user;
    ArrayList<String> key, abjad, link_, file_name;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        allowNetworkOnMainThread();
        mConn = new ConnectMySql().getConnection();

        SP_Help = new SharedPrefManager(getApplicationContext());
        id_user = SP_Help.getSPID();

        Bundle b = this.getIntent().getExtras();
        assert b != null;
        no_order = b.getString("no_ticket");
        ticket_id = b.getString("ticket_id");
        status = b.getString("status");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        inisialisasi();
        cb_setuju.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    bt_tidak_setuju.setVisibility(View.INVISIBLE);
                }else {
                    bt_tidak_setuju.setVisibility(View.VISIBLE);
                }
            }
        });
        if (status.equals("1")) {
            cb_setuju.setVisibility(View.GONE);
            bt_tidak_setuju.setVisibility(View.GONE);
            bt_approve.setVisibility(View.GONE);
            tv_sudah.setVisibility(View.VISIBLE);
        } else if (status.equals("2")) {
            cb_setuju.setVisibility(View.GONE);
            bt_tidak_setuju.setVisibility(View.GONE);
            bt_approve.setVisibility(View.GONE);
            ll_tidak.setVisibility(View.VISIBLE);
        }

        showOrder();
        showatt(ticket_id);


        bt_approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cb_setuju.isChecked()) {
                    dialogSetuju();
                } else {
                    Toast.makeText(getApplicationContext(), "Centang setuju terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            }
        });

        bt_tidak_setuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogTidakSetuju();
            }
        });

        bt_ubahApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUbah();
            }
        });

        ll_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TicketActivity.this, ActivityImg.class);
                i.putStringArrayListExtra("file_name", file_name);
                i.putStringArrayListExtra("link", link_);
                startActivity(i);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });
    }

    void dialogAlasan(String alasan, final String snackbar, final String status, final String oldstatus) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewInflate = inflater.inflate(R.layout.dialog_tidak_setuju, (ViewGroup) findViewById(android.R.id.content), false);
        final EditText et_alasan = (EditText) viewInflate.findViewById(R.id.et_alasan);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(alasan)
                .setView(viewInflate)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String s_alasan = et_alasan.getText().toString();
                        if (s_alasan.isEmpty()) {
                            Snackbar snack = Snackbar.make(findViewById(android.R.id.content), snackbar, Snackbar.LENGTH_LONG);
                            View view = snack.getView();
                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
                            view.setLayoutParams(params);
                            snack.show();
                        } else {
                            upload(id_user, ticket_id, status, s_alasan, oldstatus);
                            pb_setuju.setVisibility(View.VISIBLE);
                            runFunc();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).create().show();
    }

    void inisialisasi() {
        tv_issue_summary = (TextView) findViewById(R.id.tv_issue_summary);
        tv_issue_Purpose = (TextView) findViewById(R.id.tv_issue_Purpose);
        tv_issue_Detail = (TextView) findViewById(R.id.tv_issue_Detail);
        tv_no_order = (TextView) findViewById(R.id.tv_no_tiket);
        tv_tanggal = (TextView) findViewById(R.id.tv_tanggal);
        tv_nama = (TextView) findViewById(R.id.tv_nama_order);
        tv_email = (TextView) findViewById(R.id.tv_email_order);
        tv_voip = (TextView) findViewById(R.id.tv_voip_order);
        tv_seksi = (TextView) findViewById(R.id.tv_seksi_order);
        tv_unit = (TextView) findViewById(R.id.tv_unit_order);
        tv_departemen = (TextView) findViewById(R.id.tv_departemen_order);
        tv_sudah = (TextView) findViewById(R.id.tv_sudah);
        cb_setuju = (CheckBox) findViewById(R.id.cb_setuju);
        bt_approve = (Button) findViewById(R.id.bt_approve);
        bt_tidak_setuju = (Button) findViewById(R.id.bt_tidak_setuju);
        bt_ubahApp = (Button) findViewById(R.id.bt_ubahApp);
        pb_setuju = (ProgressBar) findViewById(R.id.pb_setuju);
        ll_tidak = (LinearLayout) findViewById(R.id.ll_tidak);
        cv_attach = (CardView) findViewById(R.id.cv_attach);
        tv_total = (TextView) findViewById(R.id.tv_total);
        ll_attach = (LinearLayout) findViewById(R.id.ll_attach);
    }

    void runFunc() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pb_setuju.setVisibility(View.GONE);
                finish();
            }
        }, 1500);
    }

    void dialogSetuju() {
        new AlertDialog.Builder(TicketActivity.this)
                .setMessage("Anda yakin ingin menyetujui Order ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        upload(id_user, ticket_id, "1", "0", "0");
                        pb_setuju.setVisibility(View.VISIBLE);
                        runFunc();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    void dialogTidakSetuju() {
        dialogAlasan("Alasan tidak setuju", "Isikan alasan tidak setuju", "2", "0");
    }

    void dialogUbah() {
        new AlertDialog.Builder(TicketActivity.this)
                .setMessage("Anda yakin ingin merubah keputusan menjadi setuju?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        update_approve(id_user, ticket_id, "1");
                        dialogAlasan("Alasan ubah keputusan", "Isikan alasan ubah keputusan", "1", "2");
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    void showOrder() {
        try {
            Statement statement = mConn.createStatement();
            String mQuery = "SELECT kt.created tanggal, kt.createdBy creator, ifnull(kt.voip_number, '-') voip_number, \n" +
                    "ke.address email, ku.isSection isSection, ku.name nama, ifnull(ku.seksi, '-') seksi, ifnull(ku.unit, '-') unit, \n" +
                    "ifnull(ku.departemen, '-') departemen, cd.subject summary, cd.purpose purpose, th.body detail \n" +
                    "FROM khs_ticket  kt, khs_user ku, khs_user_email ke, khs_ticket__cdata cd, khs_ticket_thread th\n" +
                    "WHERE kt.number = " + no_order +
                    " AND kt.user_id = ku.id\n" +
                    "AND ku.default_email_id = ke.id\n" +
                    "AND kt.ticket_id = cd.ticket_id\n" +
                    "AND kt.ticket_id = th.ticket_id\n" +
                    "AND th.id = (select min(id) from khs_ticket_thread th2 where th2.ticket_id = kt.ticket_id)\n" +
                    "AND th.thread_type = 'M'";
            ResultSet result = statement.executeQuery(mQuery);
            while (result.next()) {
                tanggal = result.getString(1);
                tanggal = tanggal.substring(0, 10);
                if (result.getString(5).equalsIgnoreCase("0")) {
                    seksi = result.getString(7);
                } else {
                    seksi = result.getString(6);
                }
                nama = result.getString(2);
                voip = result.getString(3);
                email = result.getString(4);
                unit = result.getString(8);
                departemen = result.getString(9);
                issue_summary = result.getString(10);
                issue_purpose = result.getString(11);
                issue_detail = result.getString(12);
                issue_detail = issue_detail.replace("<br />", "\n");
                issue_detail = issue_detail.replace("<ul>", "");
                issue_detail = issue_detail.replace("</ul>", "");
                issue_detail = issue_detail.replace("<li>", "-");
                issue_detail = issue_detail.replace("</li>", "\n");
                issue_detail = issue_detail.replace("&nbsp;", " ");
                issue_detail = issue_detail.replace("<b>", "");
                issue_detail = issue_detail.replace("</b>", "");
                issue_detail = issue_detail.replace("<u>", "");
                issue_detail = issue_detail.replace("</u>", "");
                issue_detail = issue_detail.replace("<i>", "");
                issue_detail = issue_detail.replace("</i>", "");
                while (issue_detail.contains("<img")){
                    img = issue_detail.substring(issue_detail.indexOf("<"), issue_detail.indexOf(">") + 1);
                    issue_detail = issue_detail.replace(img, "");
                    Log.d("Gambar", "I");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        tv_no_order.setText("Order #" + no_order);
        tv_tanggal.setText(": " + tanggal);
        tv_nama.setText(": " + nama);
        tv_email.setText(": " + email);
        tv_voip.setText(": " + voip);
        tv_seksi.setText(": " + seksi);
        tv_unit.setText(": " + unit);
        tv_departemen.setText(": " + departemen);
        tv_issue_summary.setText(issue_summary);
        tv_issue_Detail.setText(issue_detail);
        tv_issue_Purpose.setText(issue_purpose);
    }

    void showatt(String ticket_id) {
        key = new ArrayList<>();
        abjad = new ArrayList<>();
        file_name = new ArrayList<>();
        link_ = new ArrayList<>();
        try {
            Statement statement = mConn.createStatement();
            String mQuery = "SELECT kf.key , SUBSTRING(kf.key, 1, 1) abjad, name , inline\n" +
                    " FROM khs_ticket_thread tt \n" +
                    "    INNER JOIN khs_ticket_attachment ta ON tt.id = ta.ref_id\n" +
                    "    INNER JOIN khs_file kf ON ta.file_id = kf.id\n" +
                    " WHERE tt.id = (SELECT MIN(ktt.id) FROM khs_ticket_thread ktt WHERE ktt.ticket_id = '"+ticket_id+"')";
            ResultSet result = statement.executeQuery(mQuery);
            while (result.next()) {
                key.add(result.getString(1));
                abjad.add(result.getString(2));
                file_name.add(result.getString(3));

                String attach = "http://ictsupport.quick.com/ticket/upload/attachment/"+result.getString(2)+"/"+result.getString(1);
//                String attach = "http://192.168.168.42/ticket-master/upload/attachment/"+result.getString(2)+"/"+result.getString(1);

                link_.add(attach);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        tv_total.setText(""+link_.size());
//        if (link_ != null) {
//            //                mmAdapter = new RecyclerViewAdapter(this, rute_canvassing_id, destination);
////                mRecyclerView.setAdapter(mmAdapter)
//            adapter = new ArrayAdapter<>(this, R.layout.row_img, file_name);
//            lv_attach.setAdapter(adapter);
//        }
    }

    public void upload(final String id_user, final String ticket_id, final String status, final String alasan, final String oldstatus) {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("user_id", id_user));
                nameValuePairs.add(new BasicNameValuePair("ticket_id", ticket_id));
                nameValuePairs.add(new BasicNameValuePair("status", status));
                nameValuePairs.add(new BasicNameValuePair("reason", alasan));
                nameValuePairs.add(new BasicNameValuePair("oldstatus", oldstatus));
                nameValuePairs.add(new BasicNameValuePair("device", "android"));

                Log.d("VALUE ", id_user + " " + ticket_id + " " + status);
                InputStream inputStream = null;
                String result = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.APPROVE);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();

                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());
                }

                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(inputStream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result = sb.toString();
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                result_upload = result;

            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
