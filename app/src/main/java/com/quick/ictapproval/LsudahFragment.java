package com.quick.ictapproval;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.quick.ictapproval.Adapter.RecyclerViewAdapter;
import com.quick.ictapproval.Config.ConnectMySql;
import com.quick.ictapproval.Config.SharedPrefManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class LsudahFragment extends Fragment implements
        RecyclerViewAdapter.ItemClickListener {
    private static final String TAG = "RecyclerViewFragment";
    RecyclerView mRecyclerView;
    ArrayList<String> list_no_ticket, list_summary, list_ticket_id;
    String id_user;
    Connection mConn;
    SwipeRefreshLayout swipe_refresh;
    SharedPrefManager SP_Help;
    private RecyclerViewAdapter mmAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public LsudahFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshItems();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allowNetworkOnMainThread();

        SP_Help = new SharedPrefManager(getContext());
        id_user = SP_Help.getSPID();

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.rv_mainFrag);

        mConn = new ConnectMySql().getConnection();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_lsudah, container, false);
        rootView.setTag(TAG);
//        refreshItems();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_sudahFrag);
        swipe_refresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refreshsudah);

        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        showticket();
        onItemsLoadComplete();

        mmAdapter = new RecyclerViewAdapter(this, list_no_ticket, list_summary);
        mRecyclerView.setAdapter(mmAdapter);
    }

    void refreshItems() {
        showticket();
        onItemsLoadComplete();
    }

    void showticket() {
        list_no_ticket = new ArrayList<>();
        list_summary = new ArrayList<>();
        list_ticket_id = new ArrayList<>();

        try {
            Statement statement = mConn.createStatement();
            String mQuery = "SELECT kt.number, ktc.subject, ckta.ticket_id\n" +
                    "FROM c_khs_ticket_approval ckta\n" +
                    ", khs_ticket kt\n" +
                    ", khs_ticket__cdata ktc\n" +
                    "WHERE ckta.user_id = " + id_user +
                    " AND ckta.terpenuhi = 1\n" +
                    " AND ckta.ticket_id = kt.ticket_id\n" +
                    " AND ckta.ticket_id = ktc.ticket_id\n";
            ResultSet result = statement.executeQuery(mQuery);
            Log.d("Query", mQuery);
            while (result.next()) {
                Log.d("Get ", "data");
                list_no_ticket.add(result.getString(1));
                list_summary.add(result.getString(2));
                list_ticket_id.add(result.getString(3));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void onItemsLoadComplete() {
        if (list_no_ticket != null) {
            mmAdapter = new RecyclerViewAdapter(this, list_no_ticket, list_summary);
            mRecyclerView.setAdapter(mmAdapter);
        }

        // Stop refresh animation
        swipe_refresh.setRefreshing(false);
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        Intent i = new Intent(getActivity(), TicketActivity.class);
        Bundle c = new Bundle();

        c.putString("no_ticket", list_no_ticket.get(position));
        c.putString("ticket_id", list_ticket_id.get(position));
        c.putString("status", "1");

        i.putExtras(c);
        startActivity(i);
        getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}