package com.quick.ictapproval;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.quick.ictapproval.Config.Config;
import com.quick.ictapproval.Config.SharedPrefManager;

public class WebLoginActivity extends AppCompatActivity {
//    JavaScriptInterface JSInterface;
    String result;
    SharedPrefManager SP_Helper;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_login);

        SP_Helper = new SharedPrefManager(this);

        webView = (WebView) findViewById(R.id.webView);

        if (SP_Helper.getSPSudahLogin()) {
            startActivity(new Intent(WebLoginActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyBrowser());
//        JSInterface = new JavaScriptInterface(this);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                android.util.Log.d("WebView", consoleMessage.message());
                result = consoleMessage.message();
                if (result.length() == 1) {
                    Toast.makeText(WebLoginActivity.this, "Username atau password salah", Toast.LENGTH_SHORT).show();
                } else {
                    SP_Helper.saveSPString(SharedPrefManager.SP_ID, result);
                    SP_Helper.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
                    Intent i = new Intent(WebLoginActivity.this, MainActivity.class);
                    startActivity(i);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    finish();
                }
                return true;
            }
        });
        webView.setVerticalScrollBarEnabled(false);
//        webView.addJavascriptInterface(JSInterface, "JSInterface");
        webView.loadUrl(Config.LOGIN);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

//    public class JavaScriptInterface {
//        Context mContext;
//
//        /**
//         * Instantiate the interface and set the context
//         */
//        JavaScriptInterface(Context c) {
//            mContext = c;
//        }
//
//        @android.webkit.JavascriptInterface
//        public void changeActivity() {
//            Intent i = new Intent(WebLoginActivity.this, MainActivity.class);
//            startActivity(i);
//            finish();
//        }
//    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
