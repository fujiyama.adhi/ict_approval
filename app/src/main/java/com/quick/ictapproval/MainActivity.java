package com.quick.ictapproval;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.quick.ictapproval.Config.CheckService;
import com.quick.ictapproval.Config.ConnectMySql;
import com.quick.ictapproval.Config.SharedPrefManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MainActivity extends AppCompatActivity {
    SharedPrefManager SP_Help;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    String username, email, user_id;
    Connection mConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SP_Help = new SharedPrefManager(this);
        user_id = SP_Help.getSPID();

        allowNetworkOnMainThread();
        mConn = new ConnectMySql().getConnection();

        getName(user_id);

        viewPager = (ViewPager) findViewById(R.id.viewpager1);
        tabLayout = (TabLayout) findViewById(R.id.tabs1);
        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        startService(new Intent(getBaseContext(), CheckService.class));

        AdapterTab adapter = new AdapterTab(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }


    void getName(String user_id) {
        try {
            Statement statement = mConn.createStatement();
            String mQuery = "SELECT ku.name, ue.address FROM khs_user ku " +
                            "INNER JOIN khs_user_email ue ON ue.user_id = ku.id " +
                            "WHERE ku.id = " + user_id;
            ResultSet result = statement.executeQuery(mQuery);
            while (result.next()){
                username = result.getString(1);
                email = result.getString(2);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        startService(new Intent(getBaseContext(), CheckService.class));
//        refreshItems();
        super.onResume();
    }

    void dialogLogout() {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage("Anda yakin ingin logout?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SP_Help.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        Intent i = new Intent(MainActivity.this, WebLoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        stopService(new Intent(getBaseContext(), CheckService.class));
                        startActivity(i);
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    void allowNetworkOnMainThread() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Username : "+ username +"\n"+"Email :" + email)
                        .setPositiveButton("Ok", null)
                        .create().show();
                break;
            case R.id.action_logout:
                dialogLogout();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }

    class AdapterTab extends FragmentStatePagerAdapter {
        public AdapterTab(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new LbelumFragment();
            }
            if (position == 1) {
                fragment = new LsudahFragment();
            }
            if (position == 2) {
                fragment = new LtidakFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = "Belum";
            }
            if (position == 1) {
                title = "Sudah";
            }
            if (position == 2) {
                title = "Tidak Setuju";
            }
            return title;
        }
    }
}