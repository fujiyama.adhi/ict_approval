package com.quick.ictapproval;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.quick.ictapproval.Config.Config;
import com.quick.ictapproval.Config.SharedPrefManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    String line = null;
    String result_login, username, password;

    Button bt_start;
    EditText et_username, et_password;
    ImageView logo;
    LinearLayout l_login;
    Animation fromtop, frombottom;

    SharedPrefManager SP_Helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //load();

        SP_Helper = new SharedPrefManager(this);

        bt_start = (Button) findViewById(R.id.bt_start);
        logo = (ImageView) findViewById(R.id.iv_login_logo);
        l_login = (LinearLayout) findViewById(R.id.layout_login);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

        fromtop = AnimationUtils.loadAnimation(this, R.anim.fromtop);
        frombottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);

        if (SP_Helper.getSPSudahLogin()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
//            finish();
        }

        logo.setAnimation(fromtop);
        l_login.setAnimation(frombottom);

        bt_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = et_username.getText().toString();
                password = et_password.getText().toString();
                login();
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }


    public void login() {
        class GetDataJSON extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

//                nameValuePairs.add(new BasicNameValuePair("__CSRFToken__", result_load));
                nameValuePairs.add(new BasicNameValuePair("luser", username));
                nameValuePairs.add(new BasicNameValuePair("lpasswd", password));

                InputStream inputStream = null;
                String result = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(Config.LOGIN);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();

                    inputStream = entity.getContent();

                    Log.e("pass 1", "connection success ");
                } catch (Exception e) {
                    Log.e("Fail 1", e.toString());

                }

                try {
                    BufferedReader reader = new BufferedReader
                            (new InputStreamReader(inputStream, "iso-8859-1"), 8);
                    StringBuilder sb = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    inputStream.close();
                    result = sb.toString();
                    result_login = result;
                    Log.e("pass 2", "connection success : " + result);
                } catch (Exception e) {
                    Log.e("Fail 2", e.toString());
                }

                Log.e("Result :", result);
                try {

                } catch (Exception e) {
                    Log.e("Fail 3", e.toString());
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                result_login = result;

//                action();
            }
        }
        GetDataJSON g = new GetDataJSON();
        g.execute();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda yakin ingin menutup aplikasi ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Tidak", null)
                .show();
    }
}
