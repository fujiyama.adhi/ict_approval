package com.quick.ictapproval.Config;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.quick.ictapproval.LoginActivity;
import com.quick.ictapproval.MainActivity;
import com.quick.ictapproval.R;
import com.quick.ictapproval.WebLoginActivity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CheckService extends Service {
    ArrayList<String> list_status_save, list_status_new;
    String id_user;
    int jumlah = 0;
    boolean flag = false;
    Connection mConn;
    Runnable run;
    Handler handler;
    SharedPrefManager SP_Help;

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SP_Help = new SharedPrefManager(this);
        checkorder();
        handler = new Handler();
        runFunc();
    }

    void runFunc() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkorder();
                run = this;
                handler.postDelayed(run, 300000);
            }
        }, 5000);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        id_user = SP_Help.getSPID();

        Log.d("ID_USER ", id_user);
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    void checkorder() {
        list_status_new = new ArrayList<String>();

        mConn = new ConnectMySql().getConnection();

        if (mConn == null && !flag) {
            notifForeground("Disconnect");
            flag = true;
        } else if (mConn == null && flag) {

        } else {
            flag = false;
            notifForeground("Connected");
            try {
                Statement statement = mConn.createStatement();
                String mQuery = "SELECT cta.ticket_id\n" +
                        "FROM c_khs_ticket_approval cta\n" +
                        "WHERE cta.user_id = " + id_user + " AND cta.terpenuhi = 0";
                Log.d("Query", mQuery);
                ResultSet result = statement.executeQuery(mQuery);
                while (result.next()) {
                    list_status_new.add(result.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (list_status_save != null) {
                if (list_status_save.equals(list_status_new)) {
                    list_status_save = list_status_new;
                }
                else {
                    if(list_status_save.size() > list_status_new.size()){
                        list_status_save = list_status_new;
                    } else if (list_status_save.size() < list_status_new.size()){
                        jumlah = jumlah + 1;
                        addNotification();
                        list_status_save = list_status_new;
                    } else {

                    }
                }
            } else {
                list_status_save = list_status_new;
            }
            Log.d("SAVE", String.valueOf(list_status_save.size()));
            Log.d("NEW", String.valueOf(list_status_new.size()));
        }
    }

    private void addNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.aproveict_nitf)
                        .setContentTitle("You have approval")
                        .setContentText(jumlah + " new request");

        Intent pendingIntent = new Intent(this, WebLoginActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, pendingIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        builder.setVibrate(new long[]{10, 500, 200, 200, 200, 200});
        builder.setLights(Color.RED, 3000, 3000);

        builder.setAutoCancel(true);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    private void notifForeground(String status) {
        Intent notificationIntent = new Intent(this, WebLoginActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = new android.support.v7.app.NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.aproveict_nitf)
                .setContentTitle("ICT Support Center Approval")
                .setContentText(status)
                .setContentIntent(pendingIntent).build();

        startForeground(1337, notification);
    }


}
