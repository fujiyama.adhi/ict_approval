package com.quick.ictapproval.Config;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by trial on 1/18/18.
 */

public class ConnectMySql {

    public Connection getConnection() {

        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            //dev
//            String cUrl = "jdbc:mysql://dev.quick.com:3306/ticket1";
//            String cUser = "amri";
//            String cPass = "amri";
            //prod
            String cUrl = "jdbc:mysql://ictsupport.quick.com:3306/ticket1";
            String cUser = "amri";
            String cPass = "amri";
            conn = DriverManager.getConnection(cUrl, cUser, cPass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conn != null) {
            Log.d("Status Connect: ", "CONNECTED");

        } else {
            Log.d("Status Connect: ", "FAILED");
        }
        return conn;
    }
}
