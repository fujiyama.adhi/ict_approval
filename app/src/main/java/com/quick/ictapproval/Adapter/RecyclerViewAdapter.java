package com.quick.ictapproval.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quick.ictapproval.R;

import java.util.ArrayList;

/**
 * Created by trial on 1/23/18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    final private ItemClickListener mClickListener;
    private ArrayList<String> mSummary, mOrder;

    public RecyclerViewAdapter(ItemClickListener mClickListener, ArrayList<String> Order, ArrayList<String> Summary) {
        this.mClickListener = mClickListener;
        this.mOrder = Order;
        this.mSummary = Summary;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.view_rv_order, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tv_order.setText("#" + mOrder.get(position));
        holder.tv_summary.setText(mSummary.get(position));
    }

    @Override
    public int getItemCount() {
        return mSummary.size();
    }

    public interface ItemClickListener {
        void onClick(View view, int position, boolean isLongClick);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        CardView cv_root;
        private TextView tv_summary;
        private TextView tv_order;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_order = (TextView) itemView.findViewById(R.id.tvlist_no_ticket);
            tv_summary = (TextView) itemView.findViewById(R.id.tvlist_issue);
            cv_root = (CardView) itemView.findViewById(R.id.card_view);
            cv_root.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            mClickListener.onClick(view, getAdapterPosition(), true);
            return true;
        }
    }


}
